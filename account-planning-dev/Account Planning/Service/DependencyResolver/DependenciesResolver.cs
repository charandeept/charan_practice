﻿using Com.ACSCorp.AccountPlanning.Service.IRepository;
using Com.ACSCorp.AccountPlanning.Service.IService;
using Com.ACSCorp.AccountPlanning.Service.Repository;
using Com.ACSCorp.AccountPlanning.Service.Repository.Context;
using Com.ACSCorp.AccountPlanning.Service.Service;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Com.ACSCorp.AccountPlanning.Service.DependencyResolver
{
    public static class DependenciesResolver
    {
        public static void ResolveDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddDbContext<SampleContext>(options => options.UseSqlServer(configuration.GetConnectionString("ConnectionString")));
            services.InjectServices();
            services.InjectRepositories();
        }

        internal static void InjectServices(this IServiceCollection services)
        {
            services.AddScoped<ISampleService, SampleService>();
        }

        internal static void InjectRepositories(this IServiceCollection services)
        {
            //services.AddScoped<ISampleRepository, SampleRepository>();
        }
    }
}
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DataForOverviewService } from '../shared/Services/data-for-overview.service';
import { EditModalComponent } from './edit-modal/edit-modal.component';
import { CustomerDetailsService } from './services/customer-details.service';

let vendorsData: Array<any>=[
  {"vendor":"Innova Solution","service":"devlopment"},
  {"vendor":"Innova Solution","service":"testing"},
  {"vendor":"Innova Solution","service":"requirment gathering"},
  {"vendor":"Innova Solution","service":"maintanence"},
  {"vendor":"Innova Solution1","service":"devlopment"},
  {"vendor":"Innova Solution1","service":"testing"},
  {"vendor":"Innova Solution1","service":"requirment gathering"},
  {"vendor":"Innova Solution1","service":"maintanence"},
  {"vendor":"Innova Solution2","service":"devlopment"},
  {"vendor":"Innova Solution2","service":"testing"},
  {"vendor":"Innova Solution2","service":"requirment gathering"},
  {"vendor":"Innova Solution2","service":"maintanence"},
  {"vendor":"Innova Solution3","service":"devlopment"},
  {"vendor":"Innova Solution3","service":"testing"},
  {"vendor":"Innova Solution3","service":"requirment gathering"},
  {"vendor":"Innova Solution3","service":"maintanence"},
  {"vendor":"Innova Solution4","service":"devlopment"},
  {"vendor":"Innova Solution4","service":"testing"},
  {"vendor":"Innova Solution4","service":"requirment gathering"},
  {"vendor":"Innova Solution4","service":"maintanence"}
]
@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent {

  public requestedId: number = 5;
  public userData: any;
  
  displayedColumns: string[] = ['vendors', 'service'];
  dataSource = vendorsData;
  stars= [1,2,3,4,5];
  rating=3;
 
/** Date declaration */
updatedDate= new Date();


  constructor(
    private dataService: DataForOverviewService, 
    private matdialog: MatDialog, 
    private customerdetails: CustomerDetailsService) {

    this.dataService.getConfig().subscribe((data: any) => {
      this.userData = data[this.requestedId - 1];
    })
    
  }
  ngOnInit(){
    console.log(this.customerdetails.vendors)
  }

  updateRating(r: any){
    this.rating = r;
  }

  openPopup() {
    this.matdialog.open(EditModalComponent, {
      maxWidth: '80vw',
      maxHeight: '90vh',
      height: '100%',
      width: '100%',
      data: this.userData
    });
  }
}

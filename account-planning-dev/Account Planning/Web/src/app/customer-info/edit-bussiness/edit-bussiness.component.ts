import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomerDetailsService } from '../services/customer-details.service';
@Component({
  selector: 'app-edit-bussiness',
  templateUrl: './edit-bussiness.component.html',
  styleUrls: ['./edit-bussiness.component.scss']
})
export class EditBussinessComponent implements OnInit {
  public json_data: any
  public data: any;
  /**
   * Used for input type=[number] tag to not enter the values in the input tag 
   */
  invalidChars = [
    "-",
    "+",
    "e",
  ];

  /** the following array store the option values in the respective array  */
  industryOption: any = [];
  deliveryModelOption: any = [];
  deliveryManagerOption: any = [];
  headquatersOption: any = [];
  timeZoneOption: any = [];

  /**
   * Form Group with form Control Name in bussiness form 
   */
  bussinessForm = new FormGroup({
    website: new FormControl(''),
    timeZone: new FormControl(''),
    industry: new FormControl(''),
    deliveryModel: new FormControl(''),
    companySize: new FormControl(''),
    onshoreResources: new FormControl(''),
    headquarters: new FormControl(''),
    offshoreResources: new FormControl(''),
    speciality: new FormControl(''),
    nearShore: new FormControl(''),
    clientPartner: new FormControl(''),
    deliveryManager: new FormControl(''),
  })

  /**
   * userData is passed in customerinfo component open popu method 
   * Assigning data to respective option Array  
   * @param userData : "User Data consist data selected by the user in customerInfo page "
   * @param customerDetails : CustomerDetails consist of options data should display in the front end
   */
  constructor(public formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public userData: any,
    private customerDetails: CustomerDetailsService) {
    this.industryOption = this.customerDetails.industryOption;
    this.deliveryModelOption = this.customerDetails.deliveryModelOption;
    this.deliveryManagerOption = this.customerDetails.deliveryManagerOption;
    this.headquatersOption = this.customerDetails.headquatersOption;
    this.timeZoneOption = this.customerDetails.timeZoneOption;

  }
  /**
   * UpdateProfile
   *  this method is used to call the updateprofile method  to fetch the data in the form after 1ms
   */
  ngOnInit(): void {
    setTimeout(() => {
      this.updateProfile()
    }, 100);
    // this.customerDetails.observer.subscribe((industryOption:any)=>{
    //   console.warn(industryOption)
    //   debugger
    // })
    // console.warn(this.industryOption)
  }
  /**
   * this method used to fetch data in the forms when edit button is clicked in cutomer info page
   */
  updateProfile() {
    let companySize = parseInt(this.userData.CompanySize)
    let Onshore = parseInt(this.userData.OnshoreResources)
    let Offshore = parseInt(this.userData.OffshoreResource)
    let NearShore = parseInt(this.userData.NearShore)
    let industry = this.userData?.Industry
    let deliveryModelTemp = this.userData?.DeliveryModel
    let headquaterTemp = this.userData?.Headquarters
    let timeZoneTemp = this.userData?.Timezone
    let deliveryManagerTemp = this.userData?.DeliveryManager
    this.bussinessForm.patchValue({
      website: this.userData?.Website,
      industry: industry,
      companySize: companySize,
      onshoreResources: Onshore,
      offshoreResources: Offshore,
      speciality: this.userData.Speciality,
      nearShore: NearShore,
      clientPartner: this.userData.ClientPartner,
      timeZone: timeZoneTemp,
      headquarters: headquaterTemp,
      deliveryManager: deliveryManagerTemp,
      deliveryModel: deliveryModelTemp
    });
  }

  /**
   * Method is used to check input type number should not contain the +,-,e in the form 
   * @param event : When ever a keypressed the method will call
   */
  numberValidation(event: any) {
    if (this.invalidChars.includes(event.key)) {
      event.preventDefault();
    }
  }
}

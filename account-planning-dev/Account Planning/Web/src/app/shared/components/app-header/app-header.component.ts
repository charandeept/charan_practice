//#region angular imports

import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { AuthService, EmployeeModel } from 'src/app/@core';
import { GlobalEventManagerService } from 'src/app/@core/services/event-manager/global-event-manager.service';
import { Constant } from 'src/app/shared/utilities/constant';

//#endregion angular imports

@Component({
  selector: 'app-header',
  templateUrl: './app-header.html',
  styleUrls: ['./app-header.scss']
})
export class AppHeaderComponent implements OnInit {

  //#region model properties

  public canShowNavBar: boolean = false;
  public showMenuIcon: boolean = false;
  public user: EmployeeModel = new EmployeeModel;
  public userName = "employeeName";
  showNavBar: boolean = true;
  isDisplay=false;
  title = 'accountPlanning';
  @Output() navbar = new EventEmitter(true);

  //#endregion model properties

  //#region constructor

  constructor(private eventManager: GlobalEventManagerService, private authService: AuthService) {
    //  this.eventManager.subscribeEventHandler(Constant.eventCode.menuIconToggle, this.canShowMenuIcon.bind(this));
  }

  //#endregion constructor

  //#region component events

  ngOnInit() {
    this.user = this.authService.getUserInfo();
    console.log(this.user);
  }

  //#endregion component events

  //#region service calls
  //#endregion service calls

  //#region public functions/events assoaciated with UI elements


  toggleDisplay(){
    this.isDisplay=!this.isDisplay;
  }

  public logout() {
    //this.authService.onLogoutHandler();
    window.open("https://acs-sso-accelerator.azurewebsites.net/account/logout/1009?returnUrl=http://localhost:61865/", "_self")
  }

  //#endregion public functions/events assoaciated with UI elements

  //#region private functions

  toggleNavBar() {
    this.navbar.emit(this.showNavBar = !this.showNavBar);
  }

  //#endregion private functions

}

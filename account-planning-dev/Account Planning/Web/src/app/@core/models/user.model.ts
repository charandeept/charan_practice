
export class EmployeeModel {
  //#region model properties
  public userID!: number;
  public employeeID!: string;

  public email!: string;

  public firstName!: string;
  public lastName!: string;
  public practice!: string;
  public practiceId!: number;
  public department!: string;
  public departmentId!: number;
  public grade!: string;
  public gradeId!: number;
  public level!: string;
  public levelId!: number;
  public role!: string;
  public roleId!: number;
  public isManager!: boolean;
  //#endregion model properties

  //#region constructor

  constructor() {
  }

  //#endregion constructor

}

﻿using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Models;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.IService
{
    public interface ISampleService
    {
        public Task<Result<SampleDTO>> GetById(int sampleId);

        public Task<Result<int>> AddSample(SampleDTO sample);

        public Task<Result<int>> UpdateSample(SampleDTO sample);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.IService;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Models;

using Microsoft.AspNetCore.Mvc;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.API.Controllers
{
    public class SampleController : BaseController
    {
        private readonly ISampleService _sampleService;

        public SampleController(ISampleService sampleService)
        {
            _sampleService = sampleService;
        }

        [HttpPost("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var response = await _sampleService.GetById(0);

            if (!response.IsSucceeded)
            {
                return BadRequest(response.GetErrorString());
            }

            return Ok(response.Value);
        }

        [HttpGet("Get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var response = await _sampleService.GetById(id);

            if (!response.IsSucceeded)
            {
                return BadRequest(response.GetErrorString());
            }

            return Ok(response.Value);
        }

        [HttpPost("Post")]
        public async Task<IActionResult> Post(SampleDTO sample)
        {
            var result = await _sampleService.AddSample(sample);

            if (!result.IsSucceeded)
            {
                return BadRequest(result.GetErrorString());
            }

            return Ok(result.Value);
        }

        [HttpPut("Put")]
        public async Task<IActionResult> Put(SampleDTO sample)
        {
            var result = await _sampleService.UpdateSample(sample);

            if (!result.IsSucceeded)
            {
                return BadRequest(result.GetErrorString());
            }

            return Ok(result.Value);
        }
    }
}
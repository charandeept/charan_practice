﻿using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.HttpServices;

using Microsoft.Extensions.DependencyInjection;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.ServiceExtensions
{
    public static class HttpServiceResolver
    {
        public static void AddHttpService(this IServiceCollection services)
        {
            services.AddHttpClient();
            services.AddScoped<IHttpService, HttpService>();
            services.AddScoped<IHttpServiceFactory, HttpServiceFactory>();
            services.AddScoped<IHttpHeaderService, HttpHeaderService>();
        }
    }
}
﻿namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.Enums
{
    public enum Operator
    {
        None = 0,
        Equals,
        NotEqual,
        Contains,
        GreaterThan,
        LessThan,
        GreaterThanEqual,
        LessThanEqual,
        In
    }
}
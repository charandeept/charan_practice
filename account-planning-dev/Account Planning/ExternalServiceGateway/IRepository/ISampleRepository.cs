﻿using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Models;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.IRepository
{
    public interface ISampleRepository : IBaseRepository
    {
        public Task<SampleDTO> GetById(int sampleId);

        public Task<int> AddSample(SampleDTO sample);

        public Task<int> UpdateSample(SampleDTO sample);
    }
}
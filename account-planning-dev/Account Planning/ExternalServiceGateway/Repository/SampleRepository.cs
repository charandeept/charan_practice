﻿using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.IRepository;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Models;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Repository.Context;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Repository.Models;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Repository
{
    public class SampleRepository : BaseRepository<Sample>, ISampleRepository
    {
        public SampleRepository(SampleContext dbContext) : base(dbContext)
        {
        }

        public Task<int> AddSample(SampleDTO sample)
        {
            throw new NotImplementedException();
        }

        public Task<SampleDTO> GetById(int sampleId)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateSample(SampleDTO sample)
        {
            throw new NotImplementedException();
        }
    }
}